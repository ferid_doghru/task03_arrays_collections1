package Task3.String.TwoStrings;
import java.util.Comparator;
public class StrObj implements Comparable<String> {
    private String first;
    private String second;
    StrObj(String woof,String meow){
        this.first= woof;
        this.second = meow;
    }
public String getFirst(){
        return first;
}
public String getSecond(){
        return second;
}
    @Override
    public int compareTo(String string){
        return first.compareTo(string);
}
}
