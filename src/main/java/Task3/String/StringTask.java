package Task3.String;

import java.util.NoSuchElementException;

public class StringTask {
    private String[] arr;
    private int count;
    StringTask(int ch){
        arr = new String[ch];
    }
    public String getData(int ch) {
        if (ch < arr.length) {
            return arr[ch];
        }else {
            throw new NoSuchElementException();
        }
    }
    public void addData(String string) {
        if (count < arr.length) {
            arr[count++]=string;
        }
    }
    public int getSize(){
        return arr.length;
    }
}
