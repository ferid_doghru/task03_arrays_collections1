package Generi;

public abstract class Droid {

    final static private int INITIAL_HEALTH = 50; // for example
    final static private int INCREASE = 2;

    private String name;
    private int health;
    private int attack;
    private int protection;
    private int level;

    public Droid(String name, int level) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public int getProtection() {
        return protection;
    }

    public void setProtection(int protection) {
        this.protection = protection;
    }

    public int getLevel() {
        return level;
    }


    @Override
    public String toString() {
        return "Your DroidModels\n" +
                "name: '" + name + '\'' +
                ", health: " + health +
                ", attack: " + attack +
                ", protection: " + protection +
                ", level: " + level;
    }

}