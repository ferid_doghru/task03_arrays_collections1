package Generi;


public class ProtectedDroid extends Droid {
    final static private int INITIAL_ATTACK = 6;  //for example
    final static private int INITIAL_PROTECTION = 50;

    public ProtectedDroid(String name, int level) {
        super(name, level);
        super.setAttack(INITIAL_ATTACK);
    }
}